using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace WordFinder.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void getpath_complete()
        {
            //test to see if method returns the shortest path
            List<string> dictionary = new List<string>();
            dictionary.Add("spin");
            dictionary.Add("spit");
            dictionary.Add("spat");
            dictionary.Add("spot");
            dictionary.Add("span");
            string result = wordFinder.getPath(dictionary,"spin","spot");

            Assert.IsTrue(result == "spin,spit,spot");
        }

        [TestMethod]
        public void getPath_empty()
        {
            //test to see if method returns empty when a path can't be made
            List<string> dictionary = new List<string>();
            dictionary.Add("spin");
            dictionary.Add("spit");
            dictionary.Add("spat");
            dictionary.Add("camp");
            dictionary.Add("span");

            string result = wordFinder.getPath(dictionary, "spin", "camp");

            Assert.IsTrue(result == "");
        }
    }
}
