﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WordFinder
{
    public class wordFinder
    {
        static void Main(string[] args)
        {
            //check argument length
            if(args.Length != 4)
            {
                Console.WriteLine("Invalid number of arguments. Use: [Dictionary File] [Start Word] [End Word] [Output file]");
                return;
            }

            string dictionary, start, end, resultFile, result;

            //add arguments to strings and output
            dictionary = args[0];
            start = args[1];
            end = args[2];
            resultFile = args[3];

            Console.WriteLine(dictionary + " " + start + " " + end + " " + resultFile);

            //check strings and file to see if they match the criteria
            if ((start.Length != 4) || (end.Length != 4))
            {
                Console.WriteLine("Please insert a four letter word for the start and the end");
                return;
            }

            
            if (File.Exists(dictionary) == false)
            {
                Console.WriteLine("Dictionary File not found");
                return;
            }

            //add all four letter words to a list
            Console.WriteLine("File found, Reading Dictionary...");
            List<String> allwords = new List<string>();
            StreamReader file = new StreamReader(dictionary);
            
            string reader;
            while((reader = file.ReadLine()) != null)
            {
                
                
                if(reader.Length == 4)
                {
                    allwords.Add(reader);
                    
                }
                    

            }
            file.Close();
            allwords.Sort(StringComparer.OrdinalIgnoreCase);

            //check if words are in list
            if(allwords.Contains(start, StringComparer.OrdinalIgnoreCase) == false)
            {
                Console.WriteLine("Start word not found");
                return;
            }

            if (allwords.Contains(end, StringComparer.OrdinalIgnoreCase) == false)
            {
                Console.WriteLine("End word not found");
                return;
            }



            Console.WriteLine("Both words found, Finding path...");
            



            
            //send list and letters to method to find the path
            result = getPath(allwords, start, end);

            //check result then split it and display output
            if(result == "")
            {
                Console.WriteLine("No path found");
                return;
            }
            string[] resultArr = result.Split(',');
            for(int i = 0; i < resultArr.Length;i++)
            {
                
                Console.WriteLine(resultArr[i]);
            }

            //write contents to file
            File.OpenWrite(resultFile).Dispose();

            Console.WriteLine("Path found, writing to file....");
            

            File.WriteAllLines(resultFile, resultArr);

            Console.WriteLine("Written to file");
        }

        public static string getPath(List<string> wordlist, String start, String end)
        {
            //string that will be returned
            string result = "";




            //track what level the search is at
            int level = 0;

            //map for keeping track of history
            Dictionary<int, string> map = new Dictionary<int, string>();

            //blocklist for all history already viewed
            List<String> blocklist = new List<string>();

            //temporary history list
            List<String> tempHist = new List<string>();

            string curStr = start;

            int index = 1;
            string history = "";

            curStr = curStr.ToLower();
            bool complete = false;

            //loop until the search is complete
            while (complete == false)
            {
                
                char[] curToChar = curStr.ToCharArray();
                //loop through each character in the word with each letter in the alphabet
                for (int i = curStr.Length - 1; i >= 0; i--)
                {
                    char letter = curToChar[i];

                    for (Char c = 'a'; c <= 'z'; c++)
                    {
                        curToChar[i] = c;
                        //if result if found set complete to true and store history
                        if (String.Join("", curToChar).ToLower() == end.ToLower())
                        {
                            if(history == "")
                            {
                                
                                result = start + "," + String.Join("", curToChar);
                                
                            }
                            else
                            {
                                result = start + "," + history + "," + String.Join("", curToChar);
                            }

                            
                            result = result.ToLower();
                            complete = true;
                            return result;
                        }
                        //if a word in the search is in the dictionary, record it and add it to the history
                        if (wordlist.Contains(String.Join("", curToChar)))
                        {

                            if (level == 0)
                            {
                                if (blocklist.Contains(String.Join("", curToChar)) == false)
                                {
                                    
                                    map.Add(index, String.Join("", curToChar));
                                    blocklist.Add(String.Join("", curToChar));
                                    index++;
                                    
                                    
                                }


                            }
                            else
                            {
                                if (blocklist.Contains(history + "," + String.Join("", curToChar)) == false)
                                {
                                    //check for any duplicates in history
                                    if (history.Contains(String.Join("", curToChar)) == false)
                                    {
                                        map.Add(index, history + "," + String.Join("", curToChar));
                                        blocklist.Add(history + "," + String.Join("", curToChar));
                                        index++;
                                        
                                    }
                                }

                            }




                        }


                    }
                    //reset word to the original current string
                    curToChar = curStr.ToCharArray();
                }
                //increment map and store history
                if (complete == false)
                {
                    //move up in map

                    

                    level++;
                    //return if map is full
                    if (level > map.Keys.Max())
                    {
                        complete = true;
                        return result;
                    }
                    history = map[level];


                    if (history.Contains(","))
                    {
                        string[] splitStr = history.Split(',');
                        curStr = splitStr[splitStr.Length - 1];

                        

                    }
                    else
                    {
                        curStr = history;
                    }

                }
            }
            //return final result
            return result;
        }

        
    }
    
}


